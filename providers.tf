terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "4.41.0"
    }

    linode = {
        source = "linode/linode"
        version = "1.25.2"
    }
  }
}